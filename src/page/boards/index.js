import React, { Component } from "react";
import "./style.less";
import axios from "axios";

export class Index extends Component {
  state = {
    data: [],
    flag: false,
    user: "",
    email: "",
    isCommit: false,
    commentsData: [],
    isFlag: false,
    ind: null,
  };
  componentDidMount() {
    axios.get("/api/comment").then((res) => {
      this.setState({
        commentsData: res.data.data[0],
      });
      // console.log(res.data.data[0]);
    });
  }

  //弹框-点击显示

  texBtn() {
    // console.log(localStorage.getItem("TKNE"));
    if (localStorage.getItem("TKNE")) {
      this.setState({
        flag: false,
      });
    } else {
      this.setState({
        flag: true,
      });
    }
  }

  //弹框-点击隐藏
  bouncedBtn() {
    this.setState({
      flag: false,
    });
  }
  //点击X-关闭弹框
  ShutDown() {
    this.setState({
      flag: false,
      user: "",
      email: "",
    });
  }
  //点击取消
  cancel() {
    this.setState({
      flag: false,
      user: "",
      email: "",
    });
  }
  //阻止冒泡
  bounBtn(e) {
    e.stopPropagation();
  }
  //名称-输入框
  ChangInput(e) {
    this.setState({
      [e.target.name]: e.target.value,
    });
  }
  //设置按钮
  SetUpThe() {
    let { user, email } = this.state;
    if (user === "dss" && email === "2966617717@qq.com") {
      localStorage.setItem("TKNE", JSON.stringify(user));
      //登录成功关闭弹框
      this.setState({
        flag: false,
        user: "",
        email: "",
      });
    } else {
      alert("名称或邮箱错误！");
    }
  }

  Creatindex(index) {
    this.setState({
      ind: index,
      isFlag: true,
    });
  }
  PackUpClick(index) {
    this.setState({
      ind: !index,
      isFlag: false,
    });
  }
  render() {
    const { flag, user, email, commentsData, isFlag, ind } = this.state;
    return (
      <div className="boox">
        <div className="BOARDS_Box">
          <div className="TheHead">
            <div className="container">
              <h2>留言板</h2>
              <p>
                <strong>[请勿灌水 🤖]</strong>
              </p>
              <p>
                <strong>[垃圾评论过多，欢迎提供好的过滤（标记）算法]</strong>
              </p>
            </div>
          </div>

          <div className="MessageBoard">
            <div className="comments">
              <div className="commentsTitle">评论</div>

              <div className="TheInput">
                <textarea
                  placeholder="请输入评论内容（支持 Markdown）"
                  onClick={this.texBtn.bind(this)}
                  name=""
                  id=""
                  cols="30"
                  rows="10"
                ></textarea>
                <footer>
                  <span>
                    <span className="expression">☺表情</span>
                  </span>
                  <div>
                    <button>发布</button>
                  </div>
                </footer>
              </div>

              <div className=" commentsSection">
                {commentsData.map((item, index) => {
                  return (
                    <div className={isFlag ? "CommentOnTheList" : "Comment__OnTheList"} key={index}>
                      <div className="TheUser">
                        <span className="HeadPortrait">用户</span>
                        <span className="TheNameOfThe">{item.name}</span>
                      </div>
                      <div className="published">
                        <div>{item.content}</div>

                        <div className="replyComments">
                          {" "}
                          <span>{item.email}</span> <span>{item.updateAt}</span>
                          <span className="ReplyToHighlight" onClick={() => this.Creatindex(index)}>
                            回复
                          </span>
                        </div>
                      </div>
                      <div className={ind === index ? "AccordingTo" : "hidden"}>
                        <div className="replyy">
                          <div className="TheAvatars">用户</div>
                          <textarea
                            name=""
                            id=""
                            cols="30"
                            rows="10"
                            placeholder="回复:"
                          ></textarea>
                        </div>

                        <div className="ButtonInTheList">
                          <span className="TheLabel">☺表情</span>{" "}
                          <button className="PackUp" onClick={() => this.PackUpClick(index)}>
                            收起
                          </button>
                          <button className="ReleaseTheButton">发布</button>
                        </div>
                      </div>
                    </div>
                  );
                })}
              </div>
            </div>

            {/* <div className="Recommended">
              <div>推荐阅读</div>
            </div> */}
          </div>
        </div>

        {flag ? (
          <div className="bounced_Box" onClick={this.bouncedBtn.bind(this)}>
            <div className="bounced" onClick={this.bounBtn.bind(this)}>
              <div className="bounced_title">
                <span>请设置你的消息</span>
                <span onClick={this.ShutDown.bind(this)} className="ShutDown">
                  X
                </span>
              </div>
              <div className="bounced_INPUT">
                <p>
                  <span>名称：</span>
                  <input
                    placeholder="请输入名称"
                    value={user}
                    name="user"
                    onChange={(e) => {
                      this.ChangInput(e);
                    }}
                  />
                </p>
                <p>
                  邮箱：
                  <input
                    placeholder="请输入邮箱"
                    value={email}
                    name="email"
                    onChange={(e) => {
                      this.ChangInput(e);
                    }}
                  />
                </p>
                <button onClick={this.cancel.bind(this)} className="cancel">
                  取消
                </button>
                <button onClick={this.SetUpThe.bind(this)} className="SetUpThe">
                  设置
                </button>
              </div>
            </div>
          </div>
        ) : null}
      </div>
    );
  }
}

export default Index;
