import React, { Component } from "react";
import RouterView from "../router/RouterView";
import { NavLink } from "react-router-dom";
import { dark, light } from "../config/theme.config";
import { inject, observer } from "mobx-react";
import { FormattedMessage } from "react-intl";

import {
  SearchOutlined,
  PlusCircleOutlined,
  MinusCircleOutlined,
  // EditOutlined,
} from "@ant-design/icons";
import imgs from "../img/1.png";
@inject("titleStore")
@observer
class Home extends Component {
  componentDidMount() {
    this.refs.zh_Chclass.style.border = "none";
    this.refs.zh_Chclass.style.background = "none";
  }
  state = {
    flag: true,
    scroll_flag: true,
    isStick: true,
    ShowClick: false,
    t: 0,
  };

  handleClickColor = () => {
    this.setState(
      {
        flag: !this.state.flag,
      },
      () => {
        window.less.modifyVars(this.state.flag ? light : dark);
      }
    );
  };
  change_Home() {
    const p = this.refs.Home_scroll.scrollTop;
    console.log(p);
    let crollStep = p - this.state.t;
    this.setState({
      t: p,
    });
    if (crollStep < 0) {
      this.refs.Home_scroll_hst.style.position = "fixed";
      this.refs.Home_scroll_hst.style.background = "#fff";

      this.refs.Home_scroll_hst.style.top = "0";
      this.refs.Home_scroll_hst.style.zIndex = "1";
      if (p === 0) {
        this.refs.Home_scroll_hst.style.position = "relative";
        this.refs.Home_scroll_hst.style.top = "0";
      }
    } else {
      this.refs.Home_scroll_hst.style.position = "fixed";
      this.refs.Home_scroll_hst.style.top = "-64px";
    }
    if (this.refs.Home_scroll.scrollTop > 100) {
      this.setState({
        ShowClick: true,
      });
    } else {
      this.setState({
        ShowClick: false,
      });
    }
  }
  btn_scrollTop() {
    console.log("1234");
    this.refs.Home_scroll.scrollTop = 0;
  }
  render() {
    let { flag, ShowClick } = this.state;
    const {
      titleStore: { locale },
      routerList,
      Child,
    } = this.props;
    console.log(routerList);

    return (
      <div
        className="Home"
        ref="Home_scroll"
        onScroll={() => {
          this.change_Home();
        }}
      >
        {ShowClick ? (
          <a
            id="back"
            onClick={() => {
              this.btn_scrollTop();
            }}
          />
        ) : null}
        {this.state.scroll_flag ? (
          <div className="Home_header" ref="Home_scroll_hst">
            <img
              className="Home_header_img"
              src={imgs}
              onClick={() => {
                this.props.history.push("/home/articles");
              }}
            />
            <div className="Home_header_navlink">
              {Child.map((item, index) => {
                return (
                  <NavLink to={item.path} key={index}>
                    <FormattedMessage id={item.meta.title} />
                  </NavLink>
                );
              })}
            </div>
            <div className="Home_header_grabble">
              <div className="Home_header_grabble_icon">
                <button onClick={() => this.props.titleStore.changeLange()} ref="zh_Chclass">
                  {locale === "zh-CN" ? "中文" : "英文"}
                </button>
                {flag ? (
                  <PlusCircleOutlined onClick={this.handleClickColor} />
                ) : (
                  <MinusCircleOutlined onClick={this.handleClickColor} />
                )}
                <SearchOutlined />
              </div>
            </div>
          </div>
        ) : null}

        <div className="Home_main">
          <RouterView routerList={routerList}></RouterView>
        </div>
        <div className="Home_footer"></div>
      </div>
    );
  }
}
export default Home;
