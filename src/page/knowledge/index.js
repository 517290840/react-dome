import React, { Component } from "react";
import "./knowledge.less";
import api from "../../api/index";
import { ShareAltOutlined, CloudOutlined } from "@ant-design/icons";
import Sidebar from "../../component/Sidebar/Sidebar";
export default class index extends Component {
  componentDidMount() {}
  state = {
    data: [],
    message: [],
    date: (
      new Date().getFullYear() +
      "-" +
      (new Date().getMonth() + 1) +
      "-" +
      new Date().getDate()
    ).replace(/\-/g, ""),
  };

  async componentDidMount() {
    let data = await api.getKnowledge();
    this.setState({
      data: data.data[0],
    });
    let res = await api.getCategory();
    this.setState({
      message: res.data,
    });
  }
  render() {
    const { data, message, date } = this.state;
    console.log(message);
    return (
      <div className="Articles">
        <div className="Articles_box">
          <div className="Articles_left">
            {data &&
              data.map((item, index) => {
                return (
                  <dl
                    key={index}
                    className="KnowledgeItem"
                    onClick={() => {
                      this.props.history.push(`/home/detail/${item.id}`);
                    }}
                  >
                    <div className="KnowledgeHeader">
                      <dt>
                        <p className="KnowledgeP">{item.title}</p>
                        <time>
                          发布于{Math.floor(date - item.publishAt.slice(0, 10).replace(/\-/g, ""))}
                          天前
                        </time>
                      </dt>
                      <dd>
                        <img src={item.cover} alt="" />
                      </dd>
                    </div>
                    <div className="KnowledgeMain">
                      <dt>{item.summary}</dt>
                      <dt
                        className="KnowledgeEye"
                        onClick={(e) => {
                          e.stopPropagation();
                        }}
                      >
                        <p>
                          <CloudOutlined />
                          {item.views}
                        </p>{" "}
                        ·{" "}
                        <p>
                          <ShareAltOutlined />
                          分享
                        </p>
                      </dt>
                    </div>
                  </dl>
                );
              })}
          </div>
          <Sidebar message={message} />
        </div>
      </div>
    );
  }
}
