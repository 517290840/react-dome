import React, { Component } from "react";
import "./index.less";
import { Carousel } from "antd";
import RouterView from "../../router/RouterView";
import { NavLink } from "react-router-dom";
import Sidebar from "../../component/Sidebar/Sidebar.js";
import "antd/dist/antd.css"; // or 'antd/dist/antd.less'
import axios from "axios";
const contentStyle = {
  height: "322px",
  color: "#fff",
  lineHeight: "322px",
  textAlign: "center",
  background: "#364d79",
};
export default class index extends Component {
  state = {
    message: [],
  };
  componentDidMount() {
    axios.get("/api/article").then((res) => {
      this.setState({
        message: res.data.data[0].splice(0, 5),
      });
    });
  }
  render() {
    const { message } = this.state;
    return (
      <div className="Articles">
        <div className="Articles_box">
          <div className="Articles_left">
            <div className="Articles_left_swiper">
              <Carousel autoplay>
                <div>
                  <h3 style={contentStyle}>1</h3>
                </div>
                <div>
                  <h3 style={contentStyle}>2</h3>
                </div>
                <div>
                  <h3 style={contentStyle}>3</h3>
                </div>
              </Carousel>
            </div>
            <div className="Articles_left_list">
              <div className="Articles_left_list_top">
                <NavLink to="/home/articles/suo">所有</NavLink>
                <NavLink to="/home/articles/qian">前端</NavLink>
                <NavLink to="/home/articles/hou">后端</NavLink>
                <NavLink to="/home/articles/yue">阅读</NavLink>
                <NavLink to="/home/articles/linux">linux</NavLink>
                <NavLink to="/home/articles/leetcode">leetcode</NavLink>
                <NavLink to="/home/articles/yao">要问</NavLink>
              </div>
              <div className="Articles_left_list_down">
                <RouterView routerList={this.props.routerList}></RouterView>
              </div>
            </div>
          </div>
          <Sidebar message={message} />
        </div>
      </div>
    );
  }
}
