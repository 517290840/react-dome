import React, { Component } from "react";
import "../children.less";
import { HeartOutlined, YoutubeOutlined, ShareAltOutlined } from "@ant-design/icons";
import axios from "axios";
class qian extends Component {
  state = { applist: [] };
  componentDidMount() {
    axios.get("/api/article").then((res) => {
      console.log(res.data.data[0]);
      this.setState({
        applist: res.data.data[0],
      });
    });
  }
  skip_detail(item) {
    this.props.history.push({
      pathname: `/home/detailvach/` + item.id,
      data: item,
    });
  }
  render() {
    return (
      <div className="articles_children">
        {this.state.applist.map((item, index) => {
          return (
            <div key={index} className="articles_children_list">
              <p
                className="articles_children_list_p"
                onClick={() => {
                  this.skip_detail(item);
                }}
              >
                <h3 className="articles_children_list_p_span">{item.title}</h3>
                <sapn className="articles_children_list_p_span">{item.publishAt}</sapn>
              </p>
              <p className="articles_children_list_box">
                <div className="articles_children_list_li">
                  <HeartOutlined />
                  {item.views}
                </div>
                <div className="articles_children_list_li">
                  <YoutubeOutlined />
                  111
                </div>
                <div className="articles_children_list_li">
                  <ShareAltOutlined />
                  分享
                </div>
              </p>
            </div>
          );
        })}
      </div>
    );
  }
}
export default qian;
