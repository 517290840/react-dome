import React, { Component } from "react";
import "./style.less";

export default class index extends Component {
  render() {
    return (
      <div className="About_Box">
        <div className="container">
          <div className="TheCover">
            <img src="https://img1.baidu.com/it/u=2512023990,1035631393&fm=26&fmt=auto" alt="" />
          </div>
          <div className="markdown">
            <h1>欢迎使用 Wipi Markdown 编辑器</h1>
            <blockquote>
              <ul>
                <li>整理知识，学习笔记</li>
                <li>发布日记，杂文，所见所想</li>
                <li>撰写发布技术文稿（代码支持）</li>
              </ul>
            </blockquote>
            <h2>什么是 Markdown</h2>
            <p>
              Markdown
              是一种方便记忆、书写的纯文本标记语言，用户可以使用这些标记符号以最小的输入代价生成极富表现力的文档：譬如您正在阅读的这份文档。它使用简单的符号标记不同的标题，分割不同的段落，
              <strong>"[粗体]"</strong>"或者"<em>斜体</em> "某些文字"
            </p>
            <h3>1. 待办事宜 Todo 列表</h3>
            <ul>
              <li className="task-list-item">
                <input type="checkbox" /> 支持以 PDF 格式导出文稿
              </li>
              <li className="task-list-item">
                <input type="checkbox" />
                新增 Todo 列表功能
              </li>
            </ul>
            <h3>2. 高亮一段代码[^code]</h3>
            <pre>
              <span className="copy-code-btn">复制</span>
              <code>
                <span className="hljs-meta">@requires_authorization</span>
                <span className="hljs-class">
                  <span className="hljs-keyword">class</span>
                  <span className="hljs-title">SomeClass</span>
                </span>
              </code>
            </pre>
            <h3>3. 绘制表格</h3>
            <table>
              <thead>
                <tr>
                  <th>项目</th>
                  <th>价格</th>
                  <th>数量</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>计算机</td>
                  <td>1600</td>
                  <td>5</td>
                </tr>
                <tr className="tabody_phone">
                  <td>手机</td>
                  <td>12</td>
                  <td>12</td>
                </tr>
                <tr>
                  <td>管线</td>
                  <td>10</td>
                  <td>234</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    );
  }
}
