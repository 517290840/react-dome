import React, { Component } from "react";
import axios from "axios";
import "./detail.less";
export default class KnowRead extends Component {
  state = {
    readBook: [],
    ids: this.props.match.params.ids,
  };
  componentDidMount() {
    console.log(this.props);
    axios.post(`/api/Knowledge/${this.props.match.params.id}/views`, {}).then((res) => {
      console.log(res);
      this.setState({
        readBook: res.data.data,
      });
    });
  }
  datailIndex(id) {
    this.setState({
      ids: id,
    });
  }
  render() {
    console.log(this.props);
    const { title } = this.props.location.state.data;
    const { child } = this.props.location.state;

    const { readBook, ids } = this.state;

    const right = child.filter((item) => {
      return item.title !== readBook.title;
    });
    return (
      <div className="Articles">
        <div className="Articles_box">
          <div className="Articles_left">
            <div className="knowRead_main">
              {child &&
                child
                  .filter((item) => item.id == ids)
                  .map((item, index) => {
                    return (
                      <div key={index}>
                        <h2
                          className="knowRead_title"
                          onClick={() => {
                            this.props.history.go(-1);
                          }}
                        >
                          知识小册 / {title} / {item.title}
                        </h2>
                        <div className="knowRead_foot">
                          <div className="knowRead_left">
                            <div className="knowRead_lefthead">
                              <h1>{item.title}</h1>
                              <p>
                                发布于 {item.publishAt} <span> 阅读量 {item.views}</span>
                              </p>
                              <p>{readBook.content}</p>
                              <p>发布于 {item.publishAt} | 版权信息:非用商-署名-自由转载</p>
                            </div>
                            <div className="knowRead_leftfoot"></div>
                          </div>
                        </div>
                      </div>
                    );
                  })}
            </div>
          </div>
          <div className="Articles_right">
            <div className="Articles_right_top">
              <div className="Articles_right_top_title">
                <h1>{title}</h1>
              </div>
              {right.map((item, index) => {
                return (
                  <li key={index} onClick={this.datailIndex.bind(this, item.id)}>
                    {item.title}
                  </li>
                );
              })}
            </div>
          </div>
        </div>
      </div>
    );
  }
}
