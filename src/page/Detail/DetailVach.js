import React, { Component } from "react";
import axios from "axios";
import "./detail.less";
import { HeartOutlined, MoreOutlined, ShareAltOutlined } from "@ant-design/icons";
export default class DetailVach extends Component {
  state = {
    applist: [],
    btn_flag: false,
  };
  componentDidMount() {
    axios.get(`/api/article/${this.props.history.location.data.id}`).then((res) => {
      this.setState({
        applist: res.data.data.content,
      });
    });
  }
  btn_number() {
    this.setState({
      btn_flag: !this.state.btn_flag,
    });
    console.log(this.state.btn_flag);
  }
  render() {
    let { applist } = this.state;
    return (
      <div className="DetailVach_box">
        <div className="DetailVach_box_like">
          <div>
            <HeartOutlined
              onClick={() => {
                this.btn_number();
              }}
              className={this.state.btn_flag ? "active_btn" : ""}
            />
          </div>
          <div>
            <MoreOutlined />
          </div>
          <div>
            <ShareAltOutlined />
          </div>
        </div>
        <div className="DetailVach_box_main" dangerouslySetInnerHTML={{ __html: applist }}></div>
      </div>
    );
  }
}
