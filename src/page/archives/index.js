import React, { Component } from "react";

import QueueAnim from "rc-queue-anim";
import style from "./style.module.less";
import AcilTiem from "../../component/Acti/ActiItem";
import Acden from "../../component/Sidebar/Acden";
import axios from "axios";
// import api from "../../api/models/article"
class Index extends Component {
  async componentDidMount() {
    await axios.get("/api/article").then((res) => {
      this.setState({
        Archivesdata: res.data.data[0],
      });
      console.log(res.data);
    });
  }

  state = {
    Archivesdata: [],
  };
  getDiataosh(id) {
    this.props.getArticleItmeListiD(id);
  }

  render() {
    let { Archivesdata } = this.state;
    console.log(Archivesdata);
    return (
      <div className={("ArchivesBox", style.ArchivesBox)}>
        <div className={("ArchivesBoxLeft", style.ArchivesBoxLeft)}>
          <div className={("ArchivesBoxLeftTop", style.ArchivesBoxLeftTop)}>
            <p className={("ArchivesBoxLeftTopPd", style.ArchivesBoxLeftTopPd)}> 归档</p>
          </div>
          <p className={("ArchivesBoxLeftText", style.ArchivesBoxLeftText)}>2021</p>
          <p className={("ArchivesBoxLeftObj", style.ArchivesBoxLeftObj)}>Archivesdata</p>

          <ul>
            <QueueAnim delay={300} type={["left"]}>
              {Archivesdata.map((item, index) => {
                return <AcilTiem key={index} item={item} />;
              })}
            </QueueAnim>
          </ul>
        </div>
        <div className={("ArchivesBoxRigth", style.ArchivesBoxRigth)}>
          <Acden message={Archivesdata} style={style} />
        </div>

        <div
          id={("ArchivesBoxStiCk", style.ArchivesBoxStiCk)}
          class="animate__animated  animate__slideInUp"
        ></div>
      </div>
    );
  }
}
export default Index;
