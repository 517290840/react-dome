import React, { Component } from "react";
import "./detail.less";
import axios from "axios";
import Sidebar from "../../component/Sidebar/Sidebar";
import api from "../../api/index";
import { message, Button } from "antd";

const warning = () => {
  message.warning("暂无阅读权限！");
};

export class index extends Component {
  state = {
    data: [],
    message: [],
    child: [],
    message: [],
    detailList: [],
  };
  async componentDidMount() {
    let res = await api.getCategory();
    console.log(res);
    this.setState({
      message: res.data,
    });
    axios.get(`/api/Knowledge/${this.props.match.params.id}`).then((res) => {
      console.log(res.data);
      this.setState({
        data: res.data.data,
        child: res.data.data.children,
      });
    });
  }
  getRead(id) {
    this.props.history.push({
      pathname: `/home/readdetail/${this.props.match.params.id}/${id}`,
      state: {
        child: this.state.child,
        data: this.state.data,
      },
    });
  }

  render() {
    console.log(this.props);
    const { id } = this.props.match.params;
    const { data, message, child } = this.state;
    console.log(child, id);
    return (
      <div className="Articles">
        <div className="Articles_box">
          <div className="Articles_left">
            {
              <div className="Detail-">
                <div className="Detail--">
                  <dl className="DetailItem">
                    <p
                      className="DetailBack"
                      onClick={() => {
                        this.props.history.go(-1);
                      }}
                    >
                      知识小册 / {data.title}
                    </p>
                    <h3>{data.title}</h3>
                    <dt>
                      <img src={data.cover} alt="" />
                    </dt>
                    <dd>
                      <h3>{data.title}</h3>
                      <p>{data.summary}</p>
                      <span>{data.views}次阅读</span>
                      <time datetime="2021-09-28 07:31:54">· 2021-09-28 07:31:54</time>
                    </dd>
                    <Button type="primary" className="btn" onClick={warning}>
                      开始阅读
                    </Button>
                    <p className="DetailP">敬请期待叭~</p>
                  </dl>
                </div>
                <div className="DetailBottom">
                  {child &&
                    child.map((item, index) => {
                      return (
                        <div key={index}>
                          <p
                            className="DetailBottom-p"
                            onClick={() => {
                              this.getRead(item.id);
                            }}
                          >
                            {item.title}
                          </p>
                        </div>
                      );
                    })}
                </div>
              </div>
            }
          </div>
          <Sidebar message={message} />
        </div>
      </div>
    );
  }
}

export default index;
