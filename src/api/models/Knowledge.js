const aslide = {
  getKnowledge: {
    url: "/api/knowledge?page=1&pageSize=12&status=publish",
    method: "get",
  },
  getKnowledgeid: {
    url: "/api/Knowledge/:id",
    method: "get",
  },
};

export default aslide;
