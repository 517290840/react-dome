import httpTools from "../until/httpAxios";
import { message } from "antd";
const context = require.context("../api/models", true, /\.js$/);
const httpAxios = httpTools({
  timeout: 10,
  commonHeaders: {
    token: window.localStorage.token,
  },
  faliMessage: (msg) => {
    message.error(msg);
  },
});
let pathObj = context.keys().reduce((prev, path) => {
  prev = { ...prev, ...context(path).default };
  return prev;
}, {});

let res = Object.keys(pathObj).reduce((prev, cur) => {
  prev[cur] = (data) => {
    return httpAxios({
      ...pathObj[cur],
      [pathObj[cur].method === "get" ? "params" : "data"]: data,
    });
  };
  return prev;
}, {});
export default res;
