import React, { Component } from "react";
import style from "./style.module.less";
import { withRouter } from "react-router-dom";
class ActiItem extends Component {
  getClkis(item) {
    this.props.history.push({
      pathname: `/home/detailvach/` + item.id,
      data: item,
    });
  }
  render() {
    let { item } = this.props;
    return (
      <div className={("actiItem", style.actiItem)} onClick={this.getClkis.bind(this, item)}>
        <div className={("ArchivesBoxLeftRedux", style.ArchivesBoxLeftRedux)}></div>
        <span>10.14</span>
        <span className={("KnowledgeP", style.KnowledgeP)}>{item.title}</span>
      </div>
    );
  }
}

export default withRouter(ActiItem);
