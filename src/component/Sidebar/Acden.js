import React, { Component } from "react";
import { ControlFilled, GithubFilled } from "@ant-design/icons";
import "../../page/knowledge/knowledge.less";
import QueueAnim from 'rc-queue-anim';
export class Sidebar extends Component {
  getClic(item) {
    console.log("i")
    this.props.history.push({
      pathname: `/home/detailvach/` + item.id,
      data: item,
    });
  }
  render() {
    const { message, style } = this.props;
    return (
      <div>
        <div className={("Articles_right_top", style.Articles_right_top)}>
          <div className={("Articles_right_top_title", style.Articles_right_top_title)}>
            <span>推荐阅读</span>
          </div>
          <div className={("Articles_right_top_content", style.Articles_right_top_content)}>
          <QueueAnim delay={300} type={[ 'left']}>
          {message.slice(6,message.length-1).map((item, index) => {
              return (
                <div key={index} onClick={this.getClic.bind(this,item)}>
                <p>{item.title}</p>
              </div>
              );
            })}
       
            </QueueAnim>
          </div>
        </div>
        <div className={("Articles_right_down", style.Articles_right_down)}>
          <div className={("Articles_right_down_title", style.Articles_right_down_title)}>
            <span>文章分类</span>
          </div>
          <div className={("Articles_right_down_content", style.Articles_right_down_content)}>
          <QueueAnim delay={300} type={[ 'left']}>
           {message.slice(0,5).map((item, index) => {
                return (
                  <div key={index} >
                    <p>{item.title}</p>
                  </div>
                );
              })}
                </QueueAnim>
          </div>
        </div>
        <div className={("Articles_right_bottom", style.Articles_right_bottom)}>
          <ControlFilled className="icon" />
          <GithubFilled className="icon" />
          <p>
            Designed by Fantasticit . 后台管理 Copyright © 2021. All Rights Reserved.
            皖ICP备18005737号
          </p>
        </div>
      </div>
    );
  }
}

export default Sidebar;
