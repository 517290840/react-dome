import React, { Component } from "react";
import { ControlFilled, GithubFilled } from "@ant-design/icons";
import "../../page/knowledge/knowledge.less";
import { withRouter } from "react-router-dom";
import QueueAnim from 'rc-queue-anim';
export class Sidebar extends Component {
  skip_detail(item) {
    this.props.history.push({
      pathname: `/home/detailvach/` + item.id,
      data: item,
    });
  }
  render() {
    const { message } = this.props;
    console.log(message)
    return (
      <div className="Articles_right">
        <div className="Articles_right_top">
          <div className="Articles_right_top_title">
            <span>推荐阅读</span>
          </div>
          <div className="Articles_right_top_content">
            <QueueAnim delay={300} type={[ 'left']}>
          {message.map((item, index) => {
              return (
                <div key={index} onClick={this.skip_detail.bind(this,item)}>
                <p>{item.title}</p>
              </div>
              );
            })}
            <br/>
            </QueueAnim>
          
          </div>
        </div>
        <div className="Articles_right_down">
          <div className="Articles_right_down_title">
            <span>文章分类</span>
          </div>
          <div className="Articles_right_down_content">
      
          {message.map((item, index) => {
                return (
                  <div key={index} class="animate__animated animate__slideInLeft" onClick={this.skip_detail.bind(this,item)}>
                    <p>{item.title}</p>
                  </div>
                );
              })}
          </div>
        </div>
        <div className="Articles_right_bottom">
          <ControlFilled className="icon" />
          <GithubFilled className="icon" />
          <p>
            Designed by Fantasticit . 后台管理 Copyright © 2021. All Rights Reserved.
            皖ICP备18005737号
          </p>
        </div>
      </div>
    );
  }
}

export default withRouter(Sidebar);
