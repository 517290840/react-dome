import React from "react";
import ReactDOM from "react-dom";
import "./styles/variables.less";
import App from "./App";
import "./index.css";
import reportWebVitals from "./reportWebVitals";
import { BrowserRouter } from "react-router-dom";
import "animate.css";
import { Provider } from "mobx-react";
import "antd/dist/antd.css"; // or 'antd/dist/antd.less'
import "animate.css";
import store from "./store/index";
ReactDOM.render(
  <Provider {...store}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Provider>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
