import loadable from "react-loadable";
const loading = () => {
  return <h1>我正在加载，别催.....再催打你</h1>;
};
const Home = loadable({ loader: () => import("../page/Home"), loading: loading });
const Articles = loadable({ loader: () => import("../page/articles/index"), loading: loading });
const Archives = loadable({ loader: () => import("../page/archives"), loading: loading });
const Knowledge = loadable({ loader: () => import("../page/knowledge"), loading: loading });
const Boards = loadable({ loader: () => import("../page/boards"), loading: loading });
const About = loadable({ loader: () => import("../page/about"), loading: loading });
const Suo = loadable({ loader: () => import("../page/articles/children/suo"), loading: loading });
const Qian = loadable({ loader: () => import("../page/articles/children/qian"), loading: loading });
const Yue = loadable({ loader: () => import("../page/articles/children/yue"), loading: loading });
const Fault = loadable({ loader: () => import("../page/Fault/Fault.js"), loading: loading });
const DetailVach = loadable({
  loader: () => import("../page/detail/DetailVach.js"),
  loading: loading,
});
const LettCode = loadable({
  loader: () => import("../page/articles/children/leetcode"),
  loading: loading,
});
const Linux = loadable({
  loader: () => import("../page/articles/children/linux"),
  loading: loading,
});
const Hou = loadable({ loader: () => import("../page/articles/children/hou"), loading: loading });
const Yao = loadable({ loader: () => import("../page/articles/children/yao"), loading: loading });

const Detail = loadable({ loader: () => import("../page/detail/index.js"), loading: loading });
const Readdetail = loadable({
  loader: () => import("../page/detail/Readdetail"),
  loading: loading,
});
const routerList = [
  { to: "/home", from: "/" },
  {
    path: "*",
    component: Fault,
  },
  {
    path: "/home",
    component: Home,
    children: [
      {
        to: "/home/articles",
        from: "/home",
      },
      {
        path: "/home/articles",
        meta: {
          title: "article",
        },
        component: Articles,
        children: [
          {
            to: "/home/articles/suo",
            from: "/home/articles",
          },
          {
            path: "/home/articles/suo",
            component: Suo,
          },
          {
            path: "/home/articles/qian",
            component: Qian,
          },
          {
            path: "/home/articles/hou",
            component: Hou,
          },
          {
            path: "/home/articles/yue",
            component: Yue,
          },
          {
            path: "/home/articles/linux",
            component: Linux,
          },
          {
            path: "/home/articles/leetcode",
            component: LettCode,
          },
          {
            path: "/home/articles/yao",
            component: Yao,
          },

          {
            path: "*",
            component: Fault,
          },
        ],
      },
      {
        path: "/home/archives",
        meta: {
          title: "archives",
        },
        component: Archives,
      },
      {
        path: "/home/knowledge",
        meta: {
          title: "knowledge",
        },
        component: Knowledge,
      },
      {
        path: "/home/boards",
        meta: {
          title: "boards",
        },
        component: Boards,
      },
      {
        path: "/home/about",
        meta: {
          title: "about",
        },
        component: About,
      },
      {
        path: "/home/detail/:id",
        component: Detail,
      },
      {
        path: "/home/detailvach/:id",
        component: DetailVach,
      },
      {
        path: "/home/readdetail/:id/:ids",
        component: Readdetail,
      },
    ],
  },
];
export default routerList;
