import { Switch, Route, Redirect } from "react-router-dom";
const RouterView = ({ routerList }) => {
  if (!routerList) {
    return null;
  }

  let redirects = routerList.filter((item) => item.to);
  console.log(redirects);
  let routerArr = routerList.filter((item) => !item.to && item.path !== "*");
  let notArr = routerList.find((item) => item.path == "*");
  return (
    <Switch>
      {routerArr.map((item, index) => {
        return (
          <Route
            key={index}
            path={item.path}
            render={(pop) => {
              if (item.children) {
                return (
                  <item.component
                    {...pop}
                    routerList={item.children.filter((item) => item.path !== "*")}
                    Child={item.children.filter(
                      (item) => !item.to && item.path !== "*" && item.meta
                    )}
                  />
                );
              }
              return <item.component {...pop} />;
            }}
          />
        );
      })}
      {redirects.map((item, index) => {
        return <Redirect key={index} from={item.from} to={item.to} />;
      })}
      {notArr && (
        <Route
          path={notArr.path}
          render={(props) => {
            return <notArr.component {...props} />;
          }}
        />
      )}
    </Switch>
  );
};

export default RouterView;
