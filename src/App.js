import React, { Component } from "react";
import RouterView from "./router/RouterView";
import routerList from "./router/routerList";
import "../src/styles/common.less";
import { IntlProvider } from "react-intl";
// import { IntlProvider } from "react-intl";
import { inject, observer } from "mobx-react";
@inject("titleStore")
@observer
class App extends Component {
  componentDidMount() {
    // this.props.getList();
    // this.props.getHomeKo();
  }
  render() {
    console.log(this.props, "88888");
    let { message, locale } = this.props.titleStore;

    return (
      <div className="App">
        <IntlProvider messages={message} locale={locale}>
          <RouterView routerList={routerList} />
        </IntlProvider>
      </div>
    );
  }
}
export default App;
