
let context = require.context("../store/modules", false, /\.js$/);
console.dir(context.keys());
let data = context.keys().reduce((prev, cur) => {

  let key = cur.match(/^\.\/(\w+)\.js$/)[1];
  let val = context(cur).default;
  prev[key] = val;
  return prev;
}, {});
console.log(data);
export default data;
