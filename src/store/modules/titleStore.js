import { makeAutoObservable } from "mobx";
import { dark, light } from "../../config/theme.config";
import zhCN from "../../lasconfig/zh-CH";
import enUS from "../../lasconfig/en-US";
const langeMessage = {
  "zh-CN": zhCN,
  "en-US": enUS,
};
class TitleStore {
  flag = true;
  locale = navigator.language;
  message = langeMessage[this.locale];
  constructor() {
    makeAutoObservable(this);
  }
  changeTheme(flag) {
    this.flag = flag;
    window.less.modifyVars(this.flag ? light : dark);
  }
  changeLange() {
    this.locale = this.locale === "zh-CN" ? "en-US" : "zh-CN";
    this.message = langeMessage[this.locale];
  }
}

export default new TitleStore();
