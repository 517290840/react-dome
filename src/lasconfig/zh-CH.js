const data = {
  article: "文章",
  archives: "归档",
  knowledge: "知识小册",
  boards: "留言板",
  about: "关于",
};

export default data;
