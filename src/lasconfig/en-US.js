const data = {
  article: "article",
  archives: "archives",
  knowledge: "knowledge",
  boards: "boards",
  about: "about",
};

export default data;
