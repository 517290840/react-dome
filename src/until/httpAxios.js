import axios from "axios";
import ErrorBoundaryUp from "./logs";
const httpTools = ({ timeout = 10, commonHeaders = {}, faliMessage }) => {
  const httpAxios = axios.create({
    baseURL: process.env.REACT_APP_BASE_URL,
    timeout: timeout * 1000,
    faliMessage: (msg) => {
      alert(msg);
    },
  });
  //请求拦截 send()之前
  httpAxios.interceptors.request.use(
    (config) => {
      console.log(config, "*******config");
      return {
        ...config,
        headers: {
          ...config.headers,
          ...commonHeaders,
        },
      };
    },
    (error) => {
      return Promise.reject(error);
    }
  );

  //响应拦截
  httpAxios.interceptors.response.use(
    (resopnse) => {
      console.log(resopnse, "*******");
      if (resopnse.data.statusCode == 200) {
        return resopnse.data;
      }
    },
    (error) => {
      console.log(error.code, "&&&&&");
      if (error.code == "ECONNABORTED") {
        faliMessage("网络超时，请刷新重试!");
      }

      ErrorBoundaryUp({
        type: "axios",
        error: {
          url: error.resopnse.config.url,
          method: error.resopnse.config.method,
        },
      });

      return Promise.reject(error);
    }
  );
  return httpAxios;
};

export default httpTools;
